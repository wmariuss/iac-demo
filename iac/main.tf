# Global variables

# General
variable "region" {
  type        = "string"
  description = "AWS region"
  default     = "us-east-1"
}

variable "project_name" {
  description = "Name of project to identify resources"
  default     = "iac-demo-tf"
}

# Network
variable "vpc_id" {
  type        = "string"
  description = "VPC ID"
  default     = "vpc-99b92fe2"
}

variable "subnet_id" {
  description = "The VPC subnet the instance(s) will go in"
  default     = "subnet-d414d69e"
}

# Compute
variable "instance_type" {
  default = "m3.medium"
}

variable "ami_id" {
  description = "The AMI to use"
  default     = "ami-43a15f3e" # Ubuntu 16.04
}

variable "number_of_instances" {
  description = "Number of instances to create"
  default     = 1
}

variable "public_key" {
 default = ""
}

variable "key_name" {
  default = "mariuss_key"
}

variable "tags" {
  default = {
    created_by = "terraform"
  }
 }


# Create EC2 instance and deploy the app

provider "aws" {
  region = "${var.region}"
}

# Security
data "aws_iam_policy_document" "default" {
  statement {
    sid = ""

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    effect = "Allow"
  }
}

resource "aws_iam_instance_profile" "default" {
  count = "${var.number_of_instances}"
  name  = "${var.project_name}-tf"
  role  = "${aws_iam_role.default.name}"
}

resource "aws_iam_role" "default" {
  count              = "${var.number_of_instances}"
  name               = "${var.project_name}-tf"
  path               = "/"
  assume_role_policy = "${data.aws_iam_policy_document.default.json}"
}

# Create EC2 instance
resource "aws_instance" "ec2_instance" {
    ami                    = "${var.ami_id}"
    iam_instance_profile   = "${aws_iam_instance_profile.default.id}" # Or you can use the existing one based on iam profile id. [aws iam list-instance-profiles]
    count                  = "${var.number_of_instances}"
    subnet_id              = "${var.subnet_id}"
    instance_type          = "${var.instance_type}"
    vpc_security_group_ids = ["${aws_security_group.public_sg.*.id}"]
    key_name               = "${aws_key_pair.ssh_key.id}" # Or using existing one. [aws ec2  describe-key-pairs]

    tags {
        created_by = "${lookup(var.tags,"created_by")}"
        Name       = "${var.project_name}-${count.index}"
    }

    # Setup basic tasks
    provisioner "salt-masterless" {
        "local_state_tree" = "salt/"
        "remote_state_tree" = "/srv/salt"
    }

    connection {
      type        = "ssh"
      user        = "ubuntu"
      private_key = "${file("~/.ssh/mariuss_git")}"
    }
}

# Add ssh key
resource "aws_key_pair" "ssh_key" {
    key_name   = "${var.key_name}"
    public_key = "${var.public_key}"
}
