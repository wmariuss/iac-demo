{% if grains['os']|lower == 'amazon' or grains['os']|lower == 'centos' or grains['os_family']|lower == 'debian' %}
install_docker:
  pkg.installed:
    - pkgs:
    {% if grains['os']|lower == 'amazon' or grains['os']|lower == 'centos' %}
      - docker
    {% elif grains['os_family']|lower == 'debian' %}
      - docker-ce
    {% endif %}
    {% if grains['os']|lower == 'centos' %}
    - watch:
      - cmd: docker_repo
  {% endif %}
{% endif %}

{% if grains['os']|lower == 'centos' %}
docker_repo:
  cmd.run:
    - names:
      - yum-config-manager add-repo https://download.docker.com/linux/centos/docker-ce.repo
      - yum-config-manager --enable docker-ce-edge
{% endif %}

{% if grains['os_family']|lower == 'debian' %}
docker_repo:
  pkgrepo.managed:
    - humanname: {{ grains["os"] }} {{ grains["oscodename"]|capitalize }} Docker Package Repository
    - name: deb [arch=amd64] https://download.docker.com/linux/{{ grains["os"]|lower }} {{ grains["oscodename"] }} stable
    - dist: {{ grains["oscodename"] }}
    - file: /etc/apt/sources.list.d/docker.list
    - keyid: 0EBFCD88
    - keyserver: pgp.mit.edu
    - refresh: True
    - require_in:
      - pkg: install_docker
{% endif %}

{% if grains['os']|lower == 'amazon' or grains['os']|lower == 'centos' or grains['os_family']|lower == 'debian' %}
docker:
  service.running:
    - enable: True
    - reload: True
    - watch:
      - pkg: install_docker
{% endif %}
