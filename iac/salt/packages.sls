util-pkgs:
  pkg.installed:
    - names:
      - apt-transport-https
      - ca-certificates
      - software-properties-common
      - debconf-utils
      - git
      - wget
      - unzip
      - python-dev
      - python-pip
      - htop
      - vim
