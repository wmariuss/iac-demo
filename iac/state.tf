terraform = {
  required_version = ">= 0.11.0"
  backend "s3" {
    bucket                  = "idemia-terraform"
    key                     = "aws/iac-demo/terraform.tfstate"
    region                  = "us-east-1"
    encrypt                 = true
    profile                 = "default"
    dynamodb_table          = "terraform-lock-table"
    shared_credentials_file = "$HOME/.aws/credentials"
  }
}
