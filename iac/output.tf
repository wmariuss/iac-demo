# Output few informations of the EC2 instance created

output "ec2_instance_id" {
  value = "${aws_instance.ec2_instance.*.id}"
}

output "public_dns_address" {
  value = "${aws_instance.ec2_instance.*.public_dns}"
}

output "public_ip_address" {
  value = "${aws_instance.ec2_instance.*.public_ip}"
}

output "private_ip_address" {
  value = "${aws_instance.ec2_instance.*.private_ip}"
}

output "ec2_instance_name" {
  value = "${aws_instance.ec2_instance.*.tags}"
}

output "security_group" {
  value = "${aws_security_group.public_sg.*.id}"
}
